const domain = 'https://reqres.in/api/'

function getAjax(url, success) {
    var xhr = window.XMLHttpRequest 
    ? new XMLHttpRequest() 
    : new ActiveXObject('Microsoft.XMLHTTP');
    // var mainīgais = nosacījums
    // 	? 1
    // 	: 2;

    // var mainīgais
    // if (nosacījums) {
    // 	mainīgais = 1;
    // } else {
    // 	mainīgais = 2;
    // }

    xhr.open('GET', domain+url);
    xhr.onreadystatechange = function() {
        if (xhr.readyState>3 && xhr.status==200) success(xhr.responseText);
    };
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.send();
    return xhr;
}

const postAjax = (url, data, success) => {
    var params = typeof data == 'string' ? data : Object.keys(data).map(
            function(k){ return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) }
        ).join('&');

    var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhr.open('POST', url);
    xhr.onreadystatechange = function() {
        if (xhr.readyState>3 && xhr.status==200) { success(xhr.responseText); }
    };
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send(params);
    return xhr;
}

// example request
// postAjax('http://foo.bar/', 'p1=1&p2=Hello+World', function(data){ console.log(data); });

// // example request with data object
// postAjax('http://foo.bar/', { p1: 1, p2: 'Hello World' }, function(data){ console.log(data); });
// // function wrapInDiv(value){
// // 	let returnValue = '<div class="col">';
// // 	returnValue += value;
// // 	returnValue += '</div>';
// // 	return returnValue;
// // }
const wrapInDiv = (value) => {
	let returnValue = '<div class="col">';
	returnValue += value;
	returnValue += '</div>';
	return returnValue;
}

const showUser = (id) => {
	alert(id);
	getAjax('users/' +id, function(data){
		result = JSON.parse(data);

	

		document.getElementById('show-user-email').innerHTML = result.data.email;

			document.getElementById('show-user-fullname')
			.innerHTML = result.data.first_name+' '+ result.data.last_name;
		// console.log(result.data);

	})
}

getAjax('users', function(data){
	// console.log(data);
	// console.log(JSON.parse(data));
	result = JSON.parse(data);
	// console.log(result.data);

	let userListPlaceholder = document.getElementById('user-list');
	
	if (result.data.length > 0) {
		document.getElementById('loading-animation').style.display = 'none';
	}
	
	Array.from(result.data).forEach(
		function(element,index){
			console.log(element, index);

			let item = '<div onclick="showUser('+element.id+')" class="row">';
			// item = item + element.id;
			item += wrapInDiv(element.id); 
			item += wrapInDiv(element.email);
			item += wrapInDiv(element.first_name);
			item += wrapInDiv(element.last_name);

			item += '</div>';
			userListPlaceholder.innerHTML += item;
		}
		);



});

window.onload = function(){
	
}


document.getElementById('create-user-subit').addEventListener('click', function(){
	alert(1);
});