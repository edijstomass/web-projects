console.log(2);

var name = 'John';
console.log(name);
console.log(name, '=', typeof name);

var age = 26;
console.log(age, '=', typeof age);

console.log(age);

console.log(typeof age);

var misc = null;

console.log(misc, '=', typeof misc);

var myObject = {
	name : 'Edijs',
	age : age,

};

console.log(myObject, '=', typeof myObject);
console.log(myObject.name);

var myArray = ['Cats', 'Dogs', 'Hedgehogs'];

console.log(myArray, '=', typeof myArray);

var flag = false;
console.log(flag, '=', typeof flag);



// let nozime to pasu ko var;
// const = tas pats, kas var tikai konstanta veertiba;

if(flag === false)
{
	console.log('flag is '+flag);
}

let check;

if(1 === '1')
{
	console.log("Yaay");
}else if( 1===1){
	console.log("Yaay as well");
}
else {
console.log("Not yaay");
}

const color = 'pink';

switch(color){
	case 'green' :
	console.log('i am green');
	break;

	case 'red' :
	console.log('red');
	break;

	default:
		console.log('not sure what i am');
}

function myFirstFunction(){
	console.log(123);
	return 'abc';
}

const test = myFirstFunction();
console.log(test);

function sum(a, b = 0){  //= 0 nozime default value (nav obligaata)
	return a+b;
}

const summa = sum(5);
console.log(summa);

const substractES5 = function substract(a,b){  // sii ir veca versija
	console.log(a-b);
}
substractES5(10,1);   // sii ir vecaa versija

const substractES6 = (a,b) => {
	console(a-b);
}

substractES5(20,1);

